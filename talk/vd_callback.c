/*
 * vd_callback.c
 *
 *  Created on: 2016年7月16日
 *      Author: hhy
 */
#include "vd_callback.h"
#include "intercom.h"
#include "my_log.h"
#include "vd_common_tools.h"
#include "vd_video.h"

void vd_recv_req_monitor_handler(device_info_t *device)
{
    log_info("vd recv %s req monitor", vd_device_info_string(device));

    //录制并发送视频数据
    vd_video_start_send();
}

void vd_recv_hang_up_monitor_handler(device_info_t *device)
{
	log_info("vd recv hang up monitor");

	vd_video_stop_send();
}

int callback_register_all(void)
{

    register_recv_req_monitor_handler(vd_recv_req_monitor_handler);
    register_inactive_hang_up_monitor_handler(vd_recv_hang_up_monitor_handler);

    return 0;
}

