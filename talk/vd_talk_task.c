/*
 * vd_talk_task.c
 *
 *  Created on: 2016年7月16日
 *      Author: hhy
 */
#include <pthread.h>

#include "vd_talk_task.h"
#include "vd_callback.h"
#include "intercom.h"
#include "my_log.h"
#include "vd_video.h"

device_info_t ui_my_device_info;
static device_info_t _vr_user = {2, 1, 1, 2, 0, DT_Room_Machine};
int vd_send_video_data_flag = 0;

int talk_task_init_task(void)
{
    int ret;

    ret = vd_video_init_record_thread();
    if(ret < 0)
        return -1;

    ret = callback_register_all();
    if(ret < 0)
        return -1;

    ret =  init_talk_task();

    return ret;
}

void *vd_thread_send_video_data(void *unused)
{
	while (vd_send_video_data_flag)
	{
	    char *buf;
	    int cnt;

		log_info("send video test");
		cnt = 2000;
		buf = malloc(cnt);
		memset(buf, 0x55, cnt+1);

		push_video_frame(cnt, buf);
		sleep(1);
	}
	return NULL;
}
void vd_create_send_video_data_thread(void)
{
    pthread_t thread_id;
    if(vd_send_video_data_flag)
        return;

    if(pthread_create(&thread_id, NULL, &vd_thread_send_video_data, NULL) != 0)
    {
        log_error("creat thread send fake video data error");
        return;
    }

    vd_send_video_data_flag = 1;
}

void vd_hang_up_monitor_handler(void)
{
	vd_send_video_data_flag = 0;
}


int vd_call_user(menu_cmd_item_t *cmd_item)
{
    if(active_call_user(&_vr_user) < 0)
    {
        return -1;
    }

    show_calling_user();

	return 0;
}
