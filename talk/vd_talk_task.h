/*
 * vd_talk_task.h
 *
 *  Created on: 2016年7月16日
 *      Author: hhy
 */
#ifndef TALK_VD_TALK_TASK_H_
#define TALK_VD_TALK_TASK_H_

#include "vd_shell_menu.h"
#include "vd_key_test.h"

int talk_task_init_task(void);
void vd_create_send_video_data_thread(void);
void vd_hang_up_monitor_handler(void);

int vd_call_user(menu_cmd_item_t *cmd_item);

#endif /* TALK_VD_TALK_TASK_H_ */
