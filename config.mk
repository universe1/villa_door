
CFLAGS += -g
CFLAGS += -Wno-int-to-pointer-cast

ifeq ($(PLATFORM), pc)
CFLAGS += -D_PC_PLATFORM
endif

ifeq ($(PLATFORM), 92755)
CFLAGS += -D_92755_QT_PLATFORM
endif

export CFLAGS
