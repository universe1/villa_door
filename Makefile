
DEPENDS_DIR = depends/

PLATFORM?=pc
PWD=$(shell pwd)
export PLATFORM

include config.mk

ifeq ($(PLATFORM),pc)
CROSS_COMPILE=
endif
ifeq ($(PLATFORM),92755)
SDK_ROOT = /home/semtec/work/sdk/northstar_equinox_qt_iu
CROSS_COMPILE=$(SDK_ROOT)/output/staging/usr/bin/arm-linux-uclibcgnueabi-
endif

CC = $(CROSS_COMPILE)gcc
AR = $(CROSS_COMPILE)ar
export CC AR

TARGET = villa_door

INC = -I$(PWD)/
INC += -I$(PWD)/tools
INC += -I$(PWD)/talk
INC += -I$(PWD)/hardware/video
ifeq ($(PLATFORM), pc)
INC += -I$(PWD)/libs/pc/include
INC += -I$(PWD)/libs/pc/include/log4c
INC += -I$(PWD)/libs/pc/include/libxml2
endif
ifeq ($(PLATFORM),92755)
INC += -I$(PWD)/libs/92755/include
INC += -I$(PWD)/libs/92755/include/log4c
INC += -I$(PWD)/libs/92755/include/libxml2
endif
export INC

LIBS = -L$(PWD)/
ifeq ($(PLATFORM), pc)
LIBS += -L$(PWD)/libs/pc/lib
endif
ifeq ($(PLATFORM), 92755)
LIBS += -L$(PWD)/libs/92755/lib
endif
export LIBS

LDFLAGS = hardware/hardware.a
ifeq ($(PLATFORM),pc)
LDFLAGS += libs/pc/lib/libIntercomCore.a
LDFLAGS += libs/pc/lib/libuv.a
LDFLAGS += -lavformat -lavcodec -lavutil -lm -lswresample -lz
endif
ifeq ($(PLATFORM),92755)
endif
LDFLAGS += -lpthread -llog4c -lxml2 -lrt -lncurses -lssl -lcares
export LDFLAGS

OBJS = main.o
OBJS += my_log.o
OBJS += vd_talk_task.o
OBJS += vd_callback.o
OBJS += vd_common_tools.o
OBJS += vd_shell_menu.o
OBJS += vd_key_test.o

OBJDIR=out
export OBJDIR

vpath %.o $(OBJDIR)
vpath %.c talk imp tools hardware/video
vpath %.d $(DEPENDS_DIR)
ifeq ($(PALTFORM), pc)
vpath %.a libs/pc/lib
endif

all: hardware_mk $(OBJS)
	@ $(CC) $(CFLAGS) $(INC) $(OBJDIR)/*.o $(LIBS) $(LDFLAGS) -o $(TARGET)
	@ echo "CC $(OBJS) $(LDFLAGS) --> $(TARGET)"

hardware_mk:
	(cd hardware && make)

%.o: %.c
	@ $(CC) $(INC) $(CFLAGS) -c $< -o $(OBJDIR)/$@
	@ echo "CC \t $@"

$(DEPENDS_DIR)%.d: %.c 
	@ $(CC) -MM $(INC) $< > $@.$$$$; \
	sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$

sinclude $(OBJS:%.o=$(DEPENDS_DIR)%.d)

clean:
	rm -f *~ *.o $(TARGET) $(OBJDIR)/*.o $(DEPENDS_DIR)/*.d
	(cd hardware && make clean)

