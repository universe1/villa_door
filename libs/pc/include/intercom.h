/** \file intercom.h
 * \brief intercom lib interface
 */

#ifndef _INTERCOM_H_
#define _INTERCOM_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <arpa/inet.h>
#include <time.h>

#include "core_log.h"

typedef enum callback_handler_type_t
{
    CALLBACK_ALARM_IN = 100,
    CALLBACK_IMP_TEXT_PUSH = 200,
    CALLBACK_IMP_AD_PUSH,
    CALLBACK_IMP_RESPONSE,
    CALLBACK_USER_ALARM_TRIGGER,
    /***********************    talk    ***********************************************/
    CALLBACK_INACTIVE_CALL = 300,
    CALLBACK_VIDEO_URL,
    CALLBACK_PLAY_VIDEO_FRAME,
    CALLBACK_AUDIO_DATA,
    CALLBACK_INACTIVE_CALL_HANG_UP,
    CALLBACK_INACTIVE_TALK_HANG_UP,
    CALLBACK_TALK_CONFIM,
    CALLBACK_CALL_RESPONSE_BUSY,
    CALLBACK_UNLOCK_CONFIRM,
    CALLBACK_UNLOCK,
    CALLBACK_CALL_ANSWER_FOUND,
    CALLBACK_TALK_ANSWER,
    CALLBACK_TALK_TASK_ERROR,
    /***********************    monitor    ***********************************************/
    CALLBACK_MONITOR_CONFIRM = 800,
    CALLBACK_INACTIVE_HANG_UP_MONITOR,
    CALLBACK_MONITOR_RESPONSE_BUSY,
    CALLBACK_REQ_MONITOR_TIMEOUT,
    CALLBACK_REQ_MONITOR,
    /***********************    fatal error    *******************************************/
    CALLBACK_FATAL_ERROR = 1000,
    /***********************    log    ***************************************************/
    CALLBACK_CORE_LOG = 1100,
}callback_handler_type_t;

typedef struct device_info_s device_info_t;

/********************************************************************************
 * intercom core
 *******************************************************************************/

/**
 * @brief 初始化楼宇对讲库核心.
 * 在初始化具体任务（比如talk task、imp task）前调用此接口初始化库
 * @param device 设备信息结构体指针，描述此设备的设备信息
 * @return 0：初始化成功 -1：初始化失败
 */
int init_intercom_core(device_info_t *device);

/**
 * @brief 销毁楼宇对讲库的所有任务.
 * 接口未实现，如果需要重启库，请重启整个程序；调用此接口不会执行任何操作
 * @return 始终返回-1
 */
int destory_intercom_core(void);

/**
 * @brief 修改设备信息.
 * 设备信息修改后（比如安装施工阶段通过ui设置设备的信息），调用此接口通知库修改此设备的信息
 * @param device 设备信息结构体指针，描述此设备的设备信息
 * @return 0：设置成功 -1：设置失败
 */
int modefy_device_info(device_info_t *device);

/********************************************************************************
 * talk task
 *******************************************************************************/
enum device_type_e
{
    DT_Room_Machine = 0,
    DT_Unit_Door_Machine,
    DT_Wall_Machine,
    DT_Secondary_Confirmation_Machine,
    DT_Center_Machine,
};

struct device_info_s
{/*结构体成员的排列顺序和网络报文相关，因此不可随便颠倒顺序*/
    int building_no;
    char unit_no;
    char layer_no;
    char room_no;
    unsigned int dev_no;
    unsigned int device_type;
};

enum talk_task_error_code_e
{//按照袁堂亮的要求，呼叫用户超时和接听超时都使用talk_task_error回调函数
    TTE_CALL_USER_TIMEOUT = 1,      //包括呼叫用户，呼叫中心管理机
    TTE_TALK_ANSWER_TIMEOUT,
};

/**
 * @brief 初始化对讲任务.
 * 初始化对讲任务前请确保已经调用init_intercom_core初始化了对讲库，并且相关注册函数已经注册
 * @return: 0: 成功；-1: 失败
 */
int init_talk_task(void);

/**
 * @brief 注册收到呼叫消息的回调函数.
 * 收到被呼叫的消息后通过此回调函数通知ui有人正在呼叫此设备
 * @param inactive_call_handler 回调函数指针，传入回调函数的参数为呼叫方的设备信息
 */
void register_inactive_call_handler(void (*inactive_call_handler)(device_info_t *));

/**
 * @brief 注册收到设备找到消息（call_answer_found）的回调函数.
 * 此设备主动呼叫其他设备时，会先搜索被呼叫的设备，
 * 被呼叫的设备收到搜索报文后会回复一条call_answer_found报文，
 * 对讲库收到对方回复的call_answer_found报文后，通过此接口通知ui设备已经找到。
 * ui可以选择忽略此消息
 *
 * @param call_answer_found_handler 回调函数指针，传入回调函数的参数为呼叫方的设备信息
 */
void register_call_answer_found_handler(void (*call_answer_found_handler)(device_info_t *));

/**
 * @brief 发送视频帧.
 * 需要发送视频的设备（目前只有门口机，但此接口对室内机同样适用）录完一个完整的H264视频帧后，通过此接口发送视频帧
 * @param len 视频帧数据长度
 * @param buf 存放视频帧数据地址的指针
 */
void push_video_frame(int len, char *buf);

/**
 * @brief 注册视频url处理函数.
 * 此接口适用于android设备和PC端室内机模拟器，92755设备请使用接口register_play_video_frame_handler
 *
 * 对讲库收到视频帧数据后，会启动rtsp服务，并通过此回调函数通知ui播放视频的url；
 * 每次对讲此回调函数最多被调用一次，ui获得url后即可通过此url播放视频
 *
 * @param url_handler 回调函数指针，传入回调函数的参数为视频地址字符串
 */
void register_video_url_handler(void (*url_handler)(char *));

/**
 * @brief 注册视频帧数据处理函数.
 * 对讲库收到完整的视频帧后，通过此回调函数将视频帧交给ui处理
 *
 * 注册此回调函数后，只使用此回调函数通知ui处理视频数据，不再建立rtsp服务，即register_video_url_handler变无效
 *
 * @param play_video_handler 回调函数指针，传入回调函数的参数为H264帧数据及其长度
 */
void register_play_video_frame_handler(void (*play_video_handler)(char *, int));

/**
 * @brief 发送音频数据.
 * android设备和PC端模拟器传入的音频数据为PCM格式，采样率16K，通道数为1，数据长度固定为960字节
 *
 * 92755设备目前只支持传入长度为180字节G722编码的音频数据，若想传入PCM数据，可统一做修改
 *
 * @param len 音频数据长度，PCM格式为960字节，G722格式为180字节
 * @param data 音频数据指针
 */
void push_audio_data(int len, char *data);

/**
 * @brief 注册音频数据处理函数.
 * 对讲库收到音频数据后通过此回调函数通知ui播放音频
 *
 * android设备和PC端模拟器回调函数的传入数据为PCM格式，采样率16K，通道数为2，数据长度固定为1920字节
 *
 * 92755设备回调函数的传入数据为G722格式，长度固定为180字节，若想传入PCM数据，可统一修改
 *
 * @param audio_handler 回调函数指针，传入参数为音频数据地址及其长度
 */
void register_audio_data_handler(void (*audio_handler)(int ,char *));

/**
 * @brief 注册呼叫过程中对方挂断的处理函数.
 * 我方主动呼叫时，如果对方在呼叫过程中点击了挂断，则我方会收到呼叫过程中对方挂断的消息。
 * 对讲库通过此回调函数通知ui对方挂断，ui应提示用户“对方不方便接听”
 * @param inactive_call_hang_up_handler 回调函数指针
 */
void register_inactive_call_hang_up_handler(void (*inactive_call_hang_up_handler)(void));

/**
 * @brief ui应答呼叫请求.
 * 被叫过程中，用户点击ui界面的“接听”后，ui通过此接口接听呼叫
 *
 * @return 0：接听成功 -1：接听失败。只有处在呼叫状态，接听都会成功，其他状态调用此接口回复失败
 */
int ui_talk_answer(void);

/**
 * @brief 注册收到对讲应答的处理函数.
 * 我方主动呼叫时，如果对方接听了呼叫，对讲库会通过此回调函数通知ui对方已接听，此时ui需要录音并发送音频数据
 * @param talk_answer_handler 回调函数指针
 */
void register_talk_answer_handler(void (*talk_answer_handler)(void));

/**
 * @brief 注册对讲确认的处理函数.
 * 我方为被叫时，用户点击“接听”后，对讲库会发送接听的报文，对方收到接听报文后回复接听确认报文，
 * 对讲库收到对方的接听确认后通过此回调函数通知ui已经成功接听，此时ui需要录音并发送音频数据
 * @param talk_confim_handler 回调函数指针
 */
void register_talk_confim_handler(void (*talk_confim_handler)(void));

/**
 * @brief 挂断呼叫.
 * 呼叫过程中，ui可通过此接口挂断，即拒绝接听
 * @return 0：挂断成功 -1：挂断失败
 */
int call_hang_up(void);

/**
 * @brief 挂断对讲.
 * 通话过程中，ui可通过此接口挂断，即挂断对讲
 * @return 0：挂断成功，-1：挂断失败
 */
int talk_hang_up(void);

/**
 * @brief 注册对方挂断回调函数.
 * 通话过程中如果对方主动挂断，则对讲服务通过此回调函数通知ui。
 * 如果达到最长通话时间仍没有收到挂断消息，对讲库也会通过此接口通知ui挂断
 * @param inactive_talk_hang_up_handler 回调函数指针
 */
void register_inactive_talk_hang_up_handler(void (*inactive_talk_hang_up_handler)(void));

/**
 * @brief 呼叫用户.
 * ui可通过此接口呼叫用户，注意：此接口是呼叫用户，即设备类型为室内机
 * @param device 设备信息结构体指针
 * @return 0：成功呼叫 -1：呼叫失败
 */
int active_call_user(device_info_t *device);

/**
 * @brief 注册用户回复忙处理函数.
 * 我方主动呼叫时，如果对方处在忙碌状态（比如正在通话中），我方会收到用户忙报文，
 * 此时对讲库通过此接口通知ui对方忙，ui需要提示用户
 * 回调函数原型：void call_busy_handler(void)
 */
void register_call_busy_handler(void (*call_busy_handler)(void));

/**
 * @brief 注册对讲任务出错处理函数.
 * 对讲过程中如果发送异常，对讲库无法处理，对讲库会通过此回调函数通知ui进行处理
 * @param error_handler 回调函数指针，传入回调函数的参数为错误码和相关设备信息，错误码参见talk_task_error_code_e
 */
void register_talk_task_error_handler(void (*error_handler)(int, device_info_t *));

/**
 * @brief 呼叫中心管理机.
 * ui可通过此接口呼叫中心管理机
 * @param addr 中心管理机ip地址
 * @return 0：成功呼叫 -1：呼叫失败
 */
int call_center_manager(struct in_addr *addr);

/** @fn unlock_door(void)
 * @brief 开锁.
 * 室内机通过此接口发送开锁命令，
 * 注意：发送开锁命令时对讲库不会检查当前状态是否能改开锁，而是直接发送开锁命令，并包含间隔500ms的5次重发机制
 */
int unlock_door(void);

/** @fn register_unlock_door_confirm_handler(void (*unlock_confirm_hanler)(int))
 * @brief 注册开锁确认回调函数.
 * 开锁成功后，室内机会收到开锁确认报文
 * @param unlock_confirm_hanler 开锁成功回调函数，参数result:1-开锁成功; 0-超时
 */
void register_unlock_door_confirm_handler(void (*unlock_confirm_hanler)(int));

/** @fn register_unlock_door_handler(void (*unlock_handler)(void))
 * @brief 注册请求开锁回调函数.
 * 门口机收到请求开锁报文后，通过此回调函数通知ui开锁
 */
void register_unlock_door_handler(void (*unlock_handler)(void));

/** @fn unlock_door_confirm(void)
 * @brief 开锁确认.
 * 门口机开锁成功后调用此接口通知室内机开锁成功
 */
int unlock_door_confirm(void);

/***********************    monitor    ***********************************************/
/**
 * @brief 监视门口机.
 * ui可通过此接口请求监视门口机和围墙机
 * @param device 被监视设备的设备信息结构体指针
 * @return 0：发送请求监视成功 -1：监视失败
 */
int ui_req_monitor(device_info_t *device);

/**
 * @brief 注册请求监视的处理函数.
 * 门口机或围墙机收到监视请求后，通过此回调函数通知ui，ui需要录像并发送视频数据
 * @param req_monitor_handler 回调函数指针，传入回调函数的参数为请求方的设备信息
 */
void register_recv_req_monitor_handler(void (*req_monitor_handler)(device_info_t *));

/**
 * @brief 注册确认监视的处理函数.
 * 我方请求监视后，门口机或围墙机回复监视确认报文，对讲库收到此报文后，通过此接口通知ui监视过程正式开始
 * @param monitor_confirm_handler 回调函数指针，传入参数为被监视设备的设备信息
 */
void register_monitor_confirm_handler(void (*monitor_confirm_handler)(device_info_t *));

/**
 * @brief 挂断监视.
 * 监视过程中，无论门口机、围墙机还是室内机，均可调用此接口挂断监视
 * @param device 对方的设备信息
 * @return 0：挂断成功 -1：挂断失败
 */
int active_hang_up_monitor(device_info_t *device);

/**
 * @brief 注册挂断监视处理函数.
 * 监视过程中，如果对发挂断监视或监视超时，对讲库通过此回调函数通知ui监视已经结束，
 * 如果我方为室内机，ui需要提示用户监视结束；
 * 如果我方为门口机或围墙机，ui需要停止录制和发送视频
 * @param hang_up_handler 回调函数指针，传入参数为对发的设备信息
 */
void register_inactive_hang_up_monitor_handler(void (*hang_up_handler)(device_info_t *));

/**
 * @brief 注册监视忙处理函数.
 * 请求监视后，如果被监视设备回复设备忙，对讲库通过此回调函数通知ui，ui需要提示用户设备忙
 * @param monitor_busy_handler 回调函数指针，传入参数为对发的设备信息
 */
void register_monitor_response_busy_handler(void (*monitor_busy_handler)(device_info_t *));

/**
 * @brief 注册请求监视超时处理函数.
 * 请求监视后，如果在超时时间内未收到被监视设备的回复信息，对讲库通过此接口通知ui。
 * 一般请求监视超时发生在没有对应的被监视设备、网络连接异常的情况下
 * @param req_monitor_timeout_handler 回调函数指针，传入参数为被监视设备的设备信息
 */
void register_req_monitor_timeout_handler(void (*req_monitor_timeout_handler)(device_info_t *));

/********************************************************************************
 * alarm task
 *******************************************************************************/
#define MAX_DEFENSE_AREA_NUM    8
typedef struct defences_switch_s
{
    int onoff;  //总开关
    char area[MAX_DEFENSE_AREA_NUM];    //1:防区开，0:防区关
}defences_switch_t;

int init_alarm_in(defences_switch_t *defence);

int modify_alarm_in_defences_area(defences_switch_t *defence);

int destory_alarm_in(void);

/*
 * 注册安防报警处理函数
 * 回调函数原型：void alarm_in_handler(int channel)
 */
int register_alarm_handler(void (*alarm_handler)(int));


/********************************************************************************
 * IMP task
 *******************************************************************************/
#define IMP_AD_PUSH_URL_MAX_LEN 1024
#define IMP_TEXT_PUSH_MAX_LEN   1536

/** @union imp_event_type_t
 * @brief IMP消息类型
 */
typedef enum imp_event_type_e
{
    IMP_ET_HEARTBEAT = 0,
    IMP_ET_CALL_RECORD = 1,
    IMP_ET_ALARM,
    IMP_ET_ELEVATOR_GUEST,          /**< 访客呼梯 */
    IMP_ET_ELEVATOR_CALL,           /**< 用户呼梯 */
    IMP_ET_CARD_EDIT,
    IMP_ET_CARD_ACCESS,
    IMP_ET_UNLOCK,
    IMP_ET_TEXT_PUSH,
    IMP_ET_AD_PUSH,
    IMP_ET_DOWNLOAD_DOC,            /**< 门口机档案下发 */
    IMP_ET_NOT_EXIST = 0xffff,
}imp_event_type_t;

enum imp_result_enum
{
    IMP_RESULT_TIMEOUT = -1,
    IMP_RESULT_OTHRE_ERROR = -2,
    IMP_RESULT_SUCCESS = 200,
    IMP_RESULT_FAILED = 480,
};

/** @enum imp_card_edit_mode_t
 * @biref 门禁卡编辑类型.
 */
typedef enum imp_card_edit_mode_e
{
    IMP_CARD_EDIT_DELETE = 0,
    IMP_CARD_EDIT_ADD = 1,
}imp_card_edit_mode_t;

/** @struct imp_ad_push_t
 * @brief IMP广告推送结构体
 */
typedef struct imp_ad_push_s
{
    int enable;                         /**< 是否启用广告，0：禁用广告；1：启用广告 */
    int play_time;                      /**< 播放时长，毫秒 */
    char url[IMP_AD_PUSH_URL_MAX_LEN];  /**< 在线广告URL */
}imp_ad_push_t;

/** @struct imp_card_edit_t
 * @brief 门禁卡编辑结构体
 */
typedef struct imp_card_edit_s
{
    unsigned int card_no;
    imp_card_edit_mode_t edit_mode;
}imp_card_edit_t;

/** @struct imp_card_edit_list_t
 * @brief 门禁卡编辑列表.
 * 协议中定义了每次最大编辑的门禁卡数量为10个
 */
typedef struct imp_card_edit_list_s
{
    int count;                          /**< 本次编辑的门禁卡数量 */
    imp_card_edit_t card_info[10];
}imp_card_edit_list_t;

/** @struct imp_heartbeat_t
 * @brief IMP心跳包结构体.
 * 心跳包中包含IMP时间，此时间可以对设备进行校时
 */
typedef struct imp_heartbeat_s
{
    int result;
    struct tm tm;
}imp_heartbeat_t;

/** @struct imp_alarm_t
 * @brief IMP安防报警结构体.
 */
typedef struct imp_alarm_s
{
    char id[20];            /**< 设备ID */
    int type;               /**< 报警类型 */
    int data;               /**< 报警状态，0：撤警 1：报警 */
}imp_alarm_t;

/** @struct imp_call_record_t
 * @brief 对讲记录结构体
 */
typedef struct imp_call_record_s
{
    device_info_t peer_info;
    int is_answer;
    int is_unlock;
}imp_call_record_t;

/** @struct imp_call_elevator_t
 * @brief 呼梯信息结构体.
 * 室内机呼梯、刷卡呼梯、访客呼梯都使用此结构体传递呼梯信息
 *
 * 访客呼梯：floor为访客楼层，即门口机楼层，一般为1层，也可能为-1层等；
 * room为呼叫的房间，包含房间楼层信息，比如呼叫的时203,则room=203,而不是3
 *
 * 室内机呼梯、刷卡呼梯：floor为呼梯楼层
 * room为呼梯房间，门口机时为0
 */
typedef struct imp_call_elevator_s
{
    int floor;
    int room;
}imp_call_elevator_t;

/** @struct imp_card_access_t
 * @brief 刷卡上报.
 * 设备产生刷卡动作时进行上报
 */
typedef struct imp_card_access_s
{
    unsigned int card;               /**< 卡号 */
}imp_card_access_t;

/** @struct imp_msg_t
 * @brief IMP消息结构体.
 */
typedef struct imp_msg_s
{
    imp_event_type_t event_type;
    int is_response;                /**< 如果是回复报文，is_response = 1，否则 is_response = 0 */
    union
    {
        int result;
        imp_ad_push_t ad;
        char text_push[IMP_TEXT_PUSH_MAX_LEN];
        imp_card_edit_list_t card_list;
        imp_heartbeat_t heartbeat;
        imp_alarm_t alarm;
        imp_call_record_t call_record;
        imp_call_elevator_t call_elev;
        imp_card_access_t card_access;
    }msg;
}imp_msg_t;

/**
 * @brief 初始化IMP任务.
 * 需要imp任务的情况下调用此接口，不使用imp时请勿初始化imp任务
 * @param imp_addr imp服务器ip地址
 * @return 0:初始化成功 -1：初始化失败
 */
int init_imp_task(struct in_addr *imp_addr);

/**
 * @brief 销毁IMP任务.
 * 销毁IMP任务的功能未实现，调用此接口不会执行任何操作
 * @return 始终返回-1
 */
int destory_imp_task(void);

/**
 * @brief 修改IMP服务器IP地址.
 * @param imp_addr imp服务器ip地址
 * @return 0:修改成功 -1：修改失败
 */
int modify_imp_addr(struct in_addr *imp_addr);

/** @fn register_recv_imp_msg_handler(void (*recv_handler)(imp_msg_t *))
 * @brief 注册接收并处理IMP消息的函数.
 * 注册此函数后，其他IMP注册函数将不再被回调，所有收到的IMP报文解码为imp_msg_t结构体后即通过此接口交由ui处理
 * @param recv_handler IMP消息处理函数
 */
int register_recv_imp_msg_handler(void (*recv_handler)(imp_msg_t *));

/** @fn send_imp_msg(imp_msg_t *msg)
 * @brief 向IMP发送消息
 * @param msg 指向imp_msg_t的结构体指针，包含准备发送的数据
 * @return -1：发送失败；0：发送成功
 */
int send_imp_msg(imp_msg_t *msg);

/*
 * IMP文本推送
 * 函数原型：void imp_text_push_handler(char *text)
 */
int register_text_push_handler(void (*text_push_handler)(char *));

/*
 * IMP广告推送
 * 回调函数原型：void imp_ad_push_handler(imp_ad_push_t *ad)
 */
int register_ad_push_handler(void (*ad_push_handler)(imp_ad_push_t *));

/*
 * IMP回复消息
 * 回调函数原型：void imp_response_handler(int event_type, int result)
 */
int register_imp_response_handler(void (*response_handler)(int, int));

/*
 * 收到报警信息（中心管理机）
 * 回调函数原型：void user_alarm_trigger_handler(device_info_t *device, int type, int data)
 */
int register_user_alarm_trigger_handler(void (*alarm_handler)(device_info_t *,int, int));

/*
 * 发送对讲记录
 */
int send_imp_call_record(device_info_t *peer_info, int is_answer, int is_unlock);

/*
 * 呼梯
 */
int send_imp_call_elevator(int floor, int room);

/*
 * 发送报警记录
 * input:type:0~n 防区类型
 *       data:0撤警，1报警
 */
int send_imp_alarm_in_record(int type, int data);

/*
 * 发送报警信息到中心管理机
 * input:addr:中心管理机地址
 *       peer_info:中心管理机设备信息
 *       type:0~n 防区类型
 *       data:0撤警，1报警
 */
int send_center_manager_alarm_record(struct in_addr *addr, device_info_t *peer_info, int type, int data);

/********************************************************************************
 * fatal error
 *******************************************************************************/
enum
{
    FE_NET_JOIN_GROUP = 1,

    FE_INIT_IMP_FAILED = 101,

    FE_INIT_ALARM_TASK_FAILED = 201,
    FE_OPEN_ALARM_DRIVER_ERROR = 202,

    FE_OTHER_ERROR,
};
/*
 * 致命错误
 * 回调函数原型：void fatal_error_handler(int error_code)
 */
int register_fatal_error_handler(void (*error_handler)(int));

#ifdef __cplusplus
}
#endif

#endif
