#ifndef _MY_LOG_H_
#define _MY_LOG_H_

#include <string.h>
#include <stdlib.h>
#include "log4c.h"


#define LOG_PRI_ERROR       LOG4C_PRIORITY_ERROR
#define LOG_PRI_WARN        LOG4C_PRIORITY_WARN
#define LOG_PRI_NOTICE      LOG4C_PRIORITY_NOTICE
#define LOG_PRI_INFO        LOG4C_PRIORITY_INFO
#define LOG_PRI_DEBUG       LOG4C_PRIORITY_DEBUG
#define LOG_PRI_TRACE       LOG4C_PRIORITY_TRACE

extern void _log_message_(int priority, const char *fmt, ...);
extern void _log_array_(const unsigned char *cmd, int cmdlen, const char *fmt);
extern void _log_trace_(const char *file, int line, const char *func, const char *fmt, ...);


/*----------------------- export interface  start ------------------------------------------------*/
                        //U can just use this interface below!
    
extern int  log_open(const char *category);
extern int  log_close();

#define log_error(fmt , args...)    _log_message_(LOG_PRI_ERROR, fmt,##args)
#define log_warn(fmt, args...)      _log_message_(LOG_PRI_WARN, fmt , ##args)
#define log_notice(fmt , args...)   _log_message_(LOG_PRI_NOTICE, fmt , ##args)
#define log_info(fmt, args...)      _log_message_(LOG_PRI_INFO, fmt, ##args)
#define log_debug(fmt , args...)    _log_message_(LOG_PRI_DEBUG, fmt,##args)
#define log_trace(fmt, args...)     _log_trace_(__FILE__ , __LINE__ , __FUNCTION__ , fmt ,##args)
#define log_array(data, len,fmt)    _log_array_(data,len,fmt)
/*----------------------- export interface  end ------------------------------------------------*/

#endif
