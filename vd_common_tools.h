/*
 * vd_common_tools.h
 *
 *  Created on: 2016年7月16日
 *      Author: hhy
 */

#ifndef VD_COMMON_TOOLS_H_
#define VD_COMMON_TOOLS_H_

#include "intercom.h"

#define ARRAY_LEN(x) (sizeof(x)/sizeof(x[0]))

char *device_info_string(device_info_t *device);

#endif /* VD_COMMON_TOOLS_H_ */
