/*
 * vd_common_tools.c
 *
 *  Created on: 2016年7月16日
 *      Author: hhy
 */
#include <string.h>
#include <stdio.h>

#include "vd_common_tools.h"

char *vd_device_info_string(device_info_t *device)
{
    /*32位机的总打印长度为40字节，64位机的总打印长度为70字节*/
    static char buf[70];

    memset(buf, 0, sizeof(buf));
    sprintf(buf, "%u-%u-%u-%u-%u-%u",
            device->device_type,
            device->building_no,
            device->unit_no,
            device->layer_no,
            device->room_no,
            device->dev_no);

    return buf;
}
