
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "my_log.h"
#include "intercom.h"
#include "vd_key_test.h"

#define DEBUG   1

void core_log_handler(int priority, char *msg)
{
    switch(priority)
    {
        case CORE_LOG_PRI_ERROR:
            log_error("%s", msg);
            break;

        case CORE_LOG_PRI_WARN:
            log_warn("%s", msg);
            break;

        case CORE_LOG_PRI_NOTICE:
            log_notice("%s", msg);
            break;

        case CORE_LOG_PRI_INFO:
            log_info("%s", msg);
            break;

        default:
            log_debug("%s", msg);
            break;
    }
}

static void _print_help(void)
{
    printf("usage: ./app_core [mode] [device info]\n");
    printf("mode:\n");
    printf("\t(none) or room: room device, use default device info 2-2-6-3-0\n");
    printf("\tdoor: door device, use default device info 2-2-0-0-2\n");
    printf("\twall: wall device, use default device info 0-0-0-0-2\n");
    printf("device info:\n");
    printf("\tbuilding-unit-layer-room-dev, example 2-2-6-3-0\n");
    printf("\troom: b-u-l-r-d or b-u-l-r or l-r\n");
    printf("\tdoor: b-u-d or d\n");
    printf("\twall: d\n");
}

unsigned int _device_type_with_name(char *type)
{
    if(strcmp(type, "room") == 0)
    {
        return DT_Room_Machine;
    }
    else if(strcmp(type, "door") == 0)
    {
        return DT_Unit_Door_Machine;
    }
    else if(strcmp(type, "wall") == 0)
    {
        return DT_Wall_Machine;
    }

    printf("unknown device type %s\n", type);
    return 255;
}

device_info_t *_get_default_device_info(char *devicename)
{
    static device_info_t device[] =
    {
            {2, 2, 6, 3, 0, DT_Room_Machine},
            {2, 2, 0, 0, 2, DT_Unit_Door_Machine},
            {0, 0, 0, 0, 2, DT_Wall_Machine},
    };
    unsigned type;

    type = _device_type_with_name(devicename);
    if(type < sizeof(device)/sizeof(device[0]))
        return &device[type];

    return NULL;
}

//device_info_t *_get_device_info_from_param(char *type, char *param)
//{
//    static device_info_t device = {0, 0, 0, 0, 0, 0};
//
//    device.device_type = _device_type_with_name(type);
//
//    if(DT_Room_Machine == device.device_type)
//    {
//        if(get_room_device_info_from_param(&device, param) == 0)
//            return NULL;
//    }
//    else if(DT_Unit_Door_Machine == device.device_type
//            || DT_Wall_Machine == device.device_type)
//    {
//        if(get_door_device_info_from_param(&device, param) == 0)
//            return NULL;
//    }
//    else
//        return NULL;
//
//    return &device;
//}

int main(int args, char *argv[])
{
	device_info_t *device;
//	struct in_addr imp_addr;

#if 0
    log_open("console");
#else
    log_open("villa_door_log");
#endif
    log_info("\n*****************\nbuild time: %s %s\n*****************", __TIME__, __DATE__);
    register_core_log_handler(core_log_handler);

    if(args <= 1)
    {
        device = _get_default_device_info("door");
    }
    else if(args <= 2)
    {
        device = _get_default_device_info(argv[1]);
    }
//    else if(args <= 3)
//    {
//        device = _get_device_info_from_param(argv[1], argv[2]);
//    }
    else
    {
        _print_help();
        return 0;
    }

    if(NULL == device)
    {
        printf("device info error\n");
        _print_help();
        return 0;
    }

    init_intercom_core(device);
    sleep(1);

//    /*imp test*/
////    imp_addr.s_addr = inet_addr("218.58.62.116");
//    imp_addr.s_addr = inet_addr("129.1.11.241");
//	init_imp_task(&imp_addr);

    talk_task_init_task();

    while(1)
    {

        sleep(1);
    }

    log_close();
    return 0;
}
