/*
 * vd_video.c
 *
 *  Created on: 2016年7月20日
 *      Author: hhy
 */
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

#include "vd_video.h"
#include "intercom.h"
#include "my_log.h"
#include "vd_video_tools.h"

#define VD_VIDEO_FARME_LEN_MAX 204800

static int _is_record_enabled = 0;
static pthread_mutex_t _is_record_enabled_mtx = PTHREAD_MUTEX_INITIALIZER;

pthread_t _record_thread_id = 0;

static char *_video_frame_buf = NULL;

static void _record_and_send_video_frame(void)
{
    int len;

    len = vd_video_file_read_frame(_video_frame_buf, VD_VIDEO_FARME_LEN_MAX);
    if(len <= 0)
        return;

    push_video_frame(len, _video_frame_buf);
}

static void *_thread_video_record(void *unused)
{
    int new = 0;
    log_info("video record thread run");

    if(NULL == _video_frame_buf)
    {
        _video_frame_buf = malloc(VD_VIDEO_FARME_LEN_MAX);
        if(NULL == _video_frame_buf)
        {
            log_error("video record thread: malloc error");
            return (void *)-1;
        }
    }

    while(1)
    {
        usleep(33000);
        if(0 == _is_record_enabled)
        {
            if(0 == new)
            {
                vd_video_file_close();
                new = 1;
            }
            continue;
        }

        if(new)
        {
            vd_video_file_open("video_test.264");
            new = 0;
        }

        _record_and_send_video_frame();
    }

    free(_video_frame_buf);
    _video_frame_buf = NULL;
    return (void *)0;
}

int vd_video_init_record_thread(void)
{
    if (pthread_create(&_record_thread_id, NULL, &_thread_video_record, NULL) != 0)
    {
        log_error("create video record failed!");
        return -1;
    }

    return 0;
}

int vd_video_start_send(void)
{
    _is_record_enabled = 1;
    return 0;
}

int vd_video_stop_send(void)
{
    _is_record_enabled = 0;
    return 0;
}
