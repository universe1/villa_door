/*
 * vd_video.h
 *
 *  Created on: 2016年7月20日
 *      Author: hhy
 */

#ifndef HARDWARE_VIDEO_VD_VIDEO_H_
#define HARDWARE_VIDEO_VD_VIDEO_H_

int vd_video_init_record_thread(void);

int vd_video_start_send(void);

int vd_video_stop_send(void);

#endif /* HARDWARE_VIDEO_VD_VIDEO_H_ */
