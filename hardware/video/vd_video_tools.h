#ifndef _VD_VIDEO_TOOLS_H_
#define _VD_VIDEO_TOOLS_H_

int vd_video_file_open(char *filename);

void vd_video_file_close(void);

int vd_video_file_read_frame(char *buf, int len);

#endif
