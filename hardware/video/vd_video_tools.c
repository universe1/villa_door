#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libavutil/avutil.h"

#include "vd_video_tools.h"

static AVFormatContext *pFormatCtx = NULL;
static int videoStream = -1;

int vd_video_file_open(char *filename)
{
    unsigned int i;

    av_register_all();

    pFormatCtx = avformat_alloc_context();

    if (avformat_open_input(&pFormatCtx, filename, NULL, NULL) != 0)
        return -1;

    if (avformat_find_stream_info(pFormatCtx, NULL) < 0)
        return -1;

    videoStream = -1;
    for (i = 0; i < pFormatCtx->nb_streams; i++)
    {
        if (pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            videoStream = i;
            break;
        }
    }
    if (videoStream == -1)
        return -1;

    return 0;
}

void vd_video_file_close(void)
{
    if(pFormatCtx != NULL)
    {
        avformat_close_input(&pFormatCtx);
    }
}

int vd_video_file_read_frame(char *buf, int len)
{
    AVPacket *packet;
    int size;

    if(NULL == pFormatCtx)
        return -1;

    packet=(AVPacket *)av_malloc(sizeof(AVPacket));

    while (av_read_frame(pFormatCtx, packet) >= 0)
    {
        if (packet->stream_index == videoStream)
        {
            size = packet->buf->size;
            if(size > len)
            {
                return -1;
            }

            memcpy(buf, packet->buf->data, size);

            av_free_packet(packet);
            return size;
        }

        av_free_packet(packet);
    }

    return 0;
}
