/*
 * vd_shell_menu.c
 *
 *  Created on: 2016年7月19日
 *      Author: semtec
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>

#include "my_log.h"
#include "intercom.h"
#include "vd_shell_menu.h"

static menu_cmd_item_t *_menu_item = NULL;
static int _item_count = 0;

int is_keypad_input(void)
{
    fd_set key_fd;
    struct timeval tv;
    int ret;

    FD_ZERO(&key_fd);
    FD_SET(0, &key_fd);
    tv.tv_sec = 0;
    tv.tv_usec = 0;

    ret = select(1, &key_fd, NULL, NULL, &tv);
    if(ret < 0)
    {
        log_error("select error");
        return 0;
    }
    else if(ret)
    {
        return 1;
    }
    return 0;
}

void init_menu(void)
{
    initscr();
    //keypad(stdscr, TRUE);
    if(has_colors())
    {
    start_color();
    //初始化颜色配对表
    init_pair(0,COLOR_BLACK,COLOR_BLACK);
    init_pair(1,COLOR_GREEN,COLOR_BLACK);
    init_pair(2,COLOR_RED,COLOR_BLACK);
    init_pair(3,COLOR_CYAN,COLOR_BLACK);
    init_pair(4,COLOR_WHITE,COLOR_BLACK);
    init_pair(5,COLOR_MAGENTA,COLOR_BLACK);
    init_pair(6,COLOR_BLUE,COLOR_BLACK);
    init_pair(7,COLOR_YELLOW,COLOR_BLACK);
    }
    attron(A_BLINK|COLOR_PAIR(3));
	clear();
}

void close_menu(void)
{
    endwin();
}

void refresh_menu(void)
{
    int i;
    char buf[32];
    device_info_t device_menu;

    clear();
    get_my_device_info(&device_menu);
    switch(device_menu.device_type)
    {
    case DT_Unit_Door_Machine:
    	addstr("\t\t*** Device door menu ***\n");break;
    case DT_Room_Machine:
    	addstr("\t\t*** Device room menu ***\n");break;
    case DT_Wall_Machine:
    	addstr("\t\t** Device machine menu **\n");break;
    default:addstr("\t\t*** Device other menu ***\n");
    }

    addstr("\t\t**** select command ****\n");

    for(i = 0; i < _item_count; i++)
    {
        sprintf(buf, "%d. ", i+1);
        addstr(buf);
        addstr(_menu_item[i].name);
        addstr(_menu_item[i].description);
    }
    addstr("please input command num:");
    refresh();
}

void show_new_menu(menu_cmd_item_t menu[], int count)
{
    _menu_item = menu;
    _item_count = count;

    refresh_menu();
}

menu_cmd_item_t *read_menu_cmd(void)
{
    menu_cmd_item_t *item = NULL;
    char buf[128];
    int cmd_index = 0;
    char *needle;

    if(!is_keypad_input())
    {
        return NULL;
    }

    getnstr(buf, sizeof(buf));
    refresh_menu();

    cmd_index = atoi(buf) - 1;
    if(cmd_index < 0 || cmd_index >= _item_count)
    {
        return NULL;
    }
    item = &_menu_item[cmd_index];

    needle = strstr(buf, " ");
    if(NULL == needle)
    {
        strcpy(item->parameter, "");
    }
    else
    {
        strncpy(item->parameter, needle+1, sizeof(item->parameter));
    }

    return item;
}

