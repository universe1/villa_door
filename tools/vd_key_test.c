/*
 * vd_key_test.c
 *
 *  Created on: 2016年7月19日
 *      Author: semtec
 */

#include "my_log.h"
#include "vd_key_test.h"
#include "vd_shell_menu.h"
#include "vd_talk_task.h"
#include "vd_common_tools.h"

static menu_cmd_item_t vd_menu_main[] =
{
        {"call room method: \n", "\t b-u-l-r or b-u-l-r-d or l-r, e.g. 1 2-2-5-5-1\n", vd_call_user},
};

static menu_cmd_item_t vd_hang_up[] =
{
        {"calling user... \n", "\n", vd_call_user},
};
void show_main_menu(void)
{
    show_new_menu(vd_menu_main, ARRAY_LEN(vd_menu_main));
}
void show_calling_user(void)
{
    show_new_menu(vd_hang_up, ARRAY_LEN(vd_hang_up));
}

void intercom_test_main_loop(void)
{
    char cmd[100] = {0};
    menu_cmd_item_t *cmd_item;

    init_menu();
    show_new_menu(vd_menu_main, ARRAY_LEN(vd_menu_main));

    while(1)
    {
        cmd_item = read_menu_cmd();
        if(NULL == cmd_item)
        {
            usleep(100000);
            continue;
        }

        log_info("cmd: %s, parameter: %s", cmd_item->name, cmd_item->parameter);
        cmd_item->cmd_handler(cmd_item);
    }

    close_menu();
}
