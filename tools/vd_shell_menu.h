/*
 * vd_shell_menu.h
 *
 *  Created on: 2016年7月19日
 *      Author: semtec
 */

#ifndef _VD_SHELL_MENU_H_
#define _VD_SHELL_MENU_H_

typedef struct menu_cmd_item_s menu_cmd_item_t;
typedef int (*cmd_handler_t)(menu_cmd_item_t *);

struct menu_cmd_item_s
{
    const char name[32];
    const char description[256];
    cmd_handler_t cmd_handler;
    char parameter[128];
};

void init_menu(void);

void close_menu(void);

void show_new_menu(menu_cmd_item_t menu[], int count);

menu_cmd_item_t *read_menu_cmd(void);

#endif /* VILLA_DOOR_TOOLS_VD_SHELL_MENU_H_ */
