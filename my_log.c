#include "log4c.h"
#include <assert.h>
#include "my_log.h"

static log4c_category_t *log_category = NULL;
static log4c_category_t *debug_category = NULL;

void _log_message_(int priority, const char *fmt, ...)
{
    va_list ap;
    assert(log_category != NULL);
    if(log4c_category_is_priority_enabled(log_category, priority))
    {
        va_start(ap, fmt);
        log4c_category_vlog(log_category, priority, fmt, ap);
        va_end(ap);
    }
}

void _log_trace_(const char *file, int line, const char *fun,
                 const char *fmt, ...)
{
    char new_fmt[2048];
    const char *head_fmt = "[%s, %d, %s]";
    va_list ap;
    int n;

    assert(log_category != NULL);
    n = sprintf(new_fmt, head_fmt, file, line, fun);
    strcat(new_fmt + n, fmt);

    va_start(ap, fmt);
    log4c_category_vlog(log_category, LOG4C_PRIORITY_TRACE, new_fmt, ap);
    va_end(ap);
}

void _log_array_(const unsigned char *cmd, int cmdlen, const char *fmt)
{

    char buff[0x100];
    int i, j, k, m;

    i = 0;
    while (cmdlen > 0)
    {
        k = cmdlen > 0x10 ? 0x10 : cmdlen;
        for (j = 0, m = 0; j < k; j++)
        {
            m += sprintf(buff + m, fmt, cmd[i + j]);
        }
        _log_message_(LOG4C_PRIORITY_DEBUG, "%s", buff);

        i += k;
        cmdlen -= k;
    }
    
}

int log_open(const char *category)
{
    if (log4c_init() == 1)
    {
        printf("Fatal Error ! Cant init log4c!\n");
        return -1;
    }

    log_category = log4c_category_get(category);
    if (NULL == log_category)
    {
        printf("Fatal Error ! Cant find category[%s]!\n",category);
        return -1;
    }

    return 0; 
}

int log_close()
{
    return (log4c_fini());
}
